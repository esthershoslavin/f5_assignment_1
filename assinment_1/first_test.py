import pytest
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.remote.webdriver import WebDriver


@pytest.fixture(scope='function')
def browser() -> WebDriver:
    driver = webdriver.Remote(
        command_executor='http://selenium__standalone-chrome:4444/wd/hub',
        options=Options())
    return driver


base_url = "https://the-internet.herokuapp.com/context_menu"


def is_text_present(browser, text):
    return str(text) in browser.page_source


def test_0(browser):
    browser.get(base_url)


def test_1(browser):
    assert(is_text_present(browser, "Right-click in the box below to see one called 'the-internet'"))


def test_2(browser):
    assert(is_text_present(browser, "Alibaba"))

